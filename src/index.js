import React from 'react';
import ReactDOM from 'react-dom';
import FlagComponent from './components/flag';
// import './index.css';
var connect = window.connectHost;
var FLAG_HOLDER_ID = 'aui-flag-container';
connect.interceptModuleActions(connect.ACTION_TYPES.FLAG_OPEN, function(options){
    var holder = document.getElementById(FLAG_HOLDER_ID);
    if(!holder){
      holder = document.createElement('div');
      holder.id = FLAG_HOLDER_ID;
      document.body.appendChild(holder);
    }
    var flag = <FlagComponent options={options} />;
    ReactDOM.render(flag, holder);
});